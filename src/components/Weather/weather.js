import React from "react";
import styles from "./weather.module.css";
import Number from "../SlideNumber/number";
export default function Weather(props) {
  return (
    <div className={styles.globalcontainer}>
      <div className={styles.container}>
        <span className={styles.day}>{props.day}</span>
        <span className={styles.temp}>
          <Number number={Math.round(props.temp - 273.15)}></Number>
        </span>
        <div className={styles.desc}>
          <div>
            <img
              src={`http://openweathermap.org/img/wn/${props.icon}.png`}
              alt="new"
            />
          </div>
          <div>{props.desc}</div>
        </div>
      </div>
    </div>
  );
}
