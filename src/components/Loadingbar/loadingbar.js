import React from "react";
import styles from "./loadingbar.module.css";
export default function Loadingbar(props) {
  return (
    <div className={styles.container}>
      <div
        className={`${styles.progress2} ${styles.progress_moved} ${styles.animationclass}`}
      >
        <div className={styles.progress_bar2}></div>
      </div>
    </div>
  );
}
