import React from "react";
import styles from "./number.module.css";
export default function Number(props) {
  return <span className={styles.fade_in}>{props.number}&#176;</span>;
}
