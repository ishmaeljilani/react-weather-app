import styles from "./App.module.css";
import Weather from "./components/Weather/weather";
import Loadingbar from "./components/Loadingbar/loadingbar";
import axios from "axios";
import React from "react";
import Number from "./components/SlideNumber/number";
//Matter of choice to adhere to 'DRY' principles can merge the two functions to the same function since only 'forecast' and 'weather' change.

function getCurrentData(setCurrentData) {
  axios
    .get(
      `https://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=${process.env.REACT_APP_WEATHER_KEY}`
    )
    .then((response) =>
      setCurrentData({
        response,
      })
    )
    .catch((error) => {
      console.log(error);
    });
}

function getFiveDayData(setFiveDayData) {
  axios
    .get(
      `https://api.openweathermap.org/data/2.5/forecast?q=London,uk&APPID=${process.env.REACT_APP_WEATHER_KEY}`
    )
    .then((response) =>
      setFiveDayData({
        response,
      })
    )
    .catch((error) => {
      console.log(error);
    });
}

function App() {
  const [currentData, setCurrentData] = React.useState(null);
  const [fiveDayData, setFiveDayData] = React.useState(null);
  const [date, setDate] = React.useState(null);
  const [day, setDay] = React.useState(null);
  const [time, setTime] = React.useState(60);
  const [remount, setRemount] = React.useState(3);

  function getData() {
    getCurrentData(setCurrentData);
    getFiveDayData(setFiveDayData);
  }

  function manageDates() {
    var tDate = new Date();
    var utcDate = tDate.toUTCString();
    setDay(tDate.getDay());
    setDate(utcDate);
  }

  var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN", "MON","TUE","WED","THU","FRI","SAT","SUN"];

  React.useEffect(() => {
    manageDates();
    getData();
    const interval = setInterval(() => {
      manageDates();
      if (time > 0) {
        setTime((s) => s - 1);
      } else {
        setTime(60);
        getData();
        setRemount((s) => s + 1);
      }
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [setDate, time]);

  return (
    <div className={styles.grid_container}>
      <div className={styles._topsection}>
        <div className={styles._location}>LONDON</div>
        <div className={styles._temprature}>
          {currentData == null ? (
            <div></div>
          ) : (
            <Number
              key={remount}
              number={Math.round(currentData.response.data.main.temp - 273.15)}
            ></Number>
          )}
        </div>
        <div className={styles._time}>
          {date == null ? <div>00:00</div> : date.slice(17, 22)} GMT
        </div>
        <div className={styles._description}>Reloading in {time}s</div>
        <div className={styles._loadingbar}>
          <Loadingbar key={remount + 1}></Loadingbar>
        </div>
      </div>
      <div className={styles._weather}>
        {fiveDayData == null ? (
          <div>Loading...</div>
        ) : (
          <div key={remount + 2}>
            {fiveDayData.response.data.list
              .filter((e) => e.dt_txt.includes("12:00:00"))
              .map((e, index) => (
                <Weather
                  key={e.dt_txt}
                  temp={e.main.temp}
                  icon={e.weather[0].icon}
                  desc={e.weather[0].description}
                  day={days[day + index + 1]}
                />
              ))}
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
